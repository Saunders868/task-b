# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### DOCUMNETATION
1. Initialised a create-react-app
2. Added folder structure
3. Added scss npm package
4. Started adding layout, scss resets, variables, mixins
5. Styled each section
6. Made utility functions
7. Initialized working BMI calculator with React State
8. Deployed using netlify and gitlab for AGILE development

# [FIGMA](https://www.figma.com/file/b5o5kQBpZbeaXK0oVIG0QM/Longevity?node-id=19%3A1077)

This project was developed mobile first and was made responsive at Mobile (320px) and Desktop (1440px) breakpoints

[Live Site](https://task-b.netlify.app)
