import Button from "./components/Button/Button";
import Layout from "./components/Layout/Layout";
import image from "./assets/rafiki.png";
import { useState } from "react";
import { BMICalculator, result } from "./utils";
import { Icon } from "@iconify/react";

function App() {
  const [formData, setFormData] = useState({
    name: "",
    height: 0,
    weight: 0,
    unitSystem: "imperial",
  });

  const [data, setData] = useState(null);
  const [interpretation, setInterpretation] = useState(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = BMICalculator(
      formData.height,
      formData.weight,
      formData.unitSystem
    );
    setData(data);
    const interpretation = result(data);
    setInterpretation(interpretation);
  };
  return (
    <>
      <Layout>
        <main className="main">
          <div className="title">
            <h1>Create a User Model</h1>
            <p>
              give us some information so we can create a user model for you
            </p>
          </div>

          <div className="hero">
            <div className="hero__image">
              <img src={image} alt="woman analysing data" />
            </div>
            <form className="form" onSubmit={handleSubmit}>
              <div className="form__group">
                <label>Name :</label>
                <input
                  className="form__group__input"
                  type="text"
                  onChange={(e) =>
                    setFormData({ ...formData, name: e.target.value })
                  }
                  required
                  placeholder="name..."
                />
              </div>
              <div className="form__group">
                <label>Unit System :</label>
                <select
                  required
                  placeholder="select..."
                  onChange={(e) =>
                    setFormData({ ...formData, unitSystem: e.target.value })
                  }
                  className="form__group__input"
                >
                  <option>imperial</option>
                  <option>metric</option>
                </select>
              </div>
              <div className="form__group">
                <label>Height :</label>
                {/* <p>Enter your height in inches eg. 6 </p> */}
                {/* <p>Enter your height in cm eg. 176 </p> */}
                <input
                  className="form__group__input"
                  type="number"
                  onChange={(e) =>
                    setFormData({ ...formData, height: e.target.value })
                  }
                  required
                  step=".01"
                  placeholder={
                    formData.unitSystem === "imperial"
                      ? "Height in inches..."
                      : "Height in m..."
                  }
                />
              </div>
              <div className="form__group">
                <label>Weight :</label>
                <input
                  className="form__group__input"
                  type="number"
                  step=".01"
                  onChange={(e) =>
                    setFormData({ ...formData, weight: e.target.value })
                  }
                  placeholder={
                    formData.unitSystem === "imperial"
                      ? "Weight in lbs..."
                      : "Weight in kg..."
                  }
                />
              </div>
              <Button />
            </form>
          </div>

          <div className="import">
            <h2 className="import__title">OR:</h2>
            <label className="import__label">
              <h3>Import</h3>
              <Icon icon="iconoir:import" color="#3683FC" width={"32"} />
              <input type="file" name="file" style={{ display: "none" }} />
            </label>
          </div>

          <div className="results">
            <h2 className="results__title">Results:</h2>
            <p className="results__text">
              {formData.name} your BMI is: {data}
            </p>
            <p className="results__text">{interpretation}</p>
          </div>
        </main>

        <footer className="footer">
        <div className="footer__wrapper">
          <p>
            <span>source: </span>
            <a href="https://www.nature.com/articles/s41598-020-69498-7#:~:text=Our%20review%20included%2032%20studies,%25)%2C%20respectively%2C%20in%20men.">
              view source <Icon icon="akar-icons:arrow-up-right" />
            </a>
          </p>
          <p>
            <span>no of datasets: </span>
            datasets were based off of the references in the study found in the
            source
          </p>
          <p>
            <span>list of scientific papers: </span>
            the sciectific papers are found in the refereces of the source
          </p>
          <p>
            <span>percentage of sensitivity: </span>
            51.4%
          </p>
          <a href="https://gitlab.com/Saunders868/task-b" rel="noreferrer" target={"_blank"}>documentation <Icon icon="akar-icons:arrow-up-right" /></a>
        </div>
      </footer>
      </Layout>
      
    </>
  );
}

export default App;
