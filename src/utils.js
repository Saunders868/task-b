export function BMICalculator(height, weight, unitSystem) {
  let BMI;
  if (unitSystem === 'imperial') {
    // weight in lbs height in inches
    const result = (weight / (height * height)) * 703
    BMI = Math.round(result * 10) / 10
  }
  if (unitSystem === 'metric') {
    // weight in kg height in m
    const result = weight / (height * height)
    BMI = Math.round(result * 10) / 10
  }

  return BMI
}

export function result(data) {
  if (data < 18.5) {
    return "you're in the underweight range";
  } else if (data >= 18.5 && data <= 24.9) {
    return "you're in the healthy weight range";
  } else if (data >= 25 && data <=29.9) {
    return "you're in the overweight range";
  } else if (data >= 30 && data <=39.9) {
    return "you're in the obese range";
  } else {
    return ""
  }
}